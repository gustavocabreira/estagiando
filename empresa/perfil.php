<?php
    include '../config.php';
    session_start();
    $codEmpresa = $_GET['cod'];
    $var_usuario_id = $_SESSION['var_usuario_id'];
    $var_usuario_menu = $_SESSION['var_usuario_menu'];

    if($var_usuario_menu == 'Assinante' || $var_usuario_id != $codEmpresa)
        $displayComentario = "block";
    else
        $displayComentario = "none";
    
    $qryEmpresa = "
        SELECT
            RazaoSocial
            , NomeFantasia
            , CNPJ
            , InscricaoEstadual
            , Email
            , FoneDDD
            , FoneNumero
            , CelularDDD
            , CelularNumero
            , Cep
            , Endereco
            , Numero
            , Bairro
            , Complemento
            , Cidade
        FROM
            empresas
        WHERE
            Id = ". $codEmpresa;
    $resultQryEmpresa = mysqli_query($con, $qryEmpresa) or die ($resultQryEmpresa);

    $fetchEmpresa = mysqli_fetch_array($resultQryEmpresa);

    $qryTotalFeedback = "
        SELECT
            count(id) as Total
            , sum(Nota) as TotalNota
        FROM
            feedback_empresa
        WHERE
            IdEmpresa = " . $codEmpresa;
    $resultQryTotalFeedback = mysqli_query($con, $qryTotalFeedback) or die (mysqli_error($resultQryTotalFeedback));
    $fetchTotalFeedback = mysqli_fetch_array($resultQryTotalFeedback);

    $media = $fetchTotalFeedback['TotalNota'] / $fetchTotalFeedback['Total'];
    $media = round($media);

    $qryFeedback = "
        SELECT
            Nome
            , Titulo
            , Comentario
            , Nota
            , fb.DataRegistro
        FROM
            feedback_empresa fb
        INNER JOIN
            Assinantes ON IdAssinante = Assinantes.Id
        WHERE
            Ativo = 1
        ORDER BY
            fb.DataRegistro DESC";
    $resultQryFeedback = mysqli_query($con, $qryFeedback) or die (mysqli_error($resultQryFeedback));

    $qryUltimoFeedBack = "
        SELECT
            Nome
            , Titulo
            , Comentario
            , Nota
            , fb.DataRegistro
        FROM
            feedback_empresa fb
        INNER JOIN
            Assinantes ON IdAssinante = Assinantes.Id
        WHERE
            Ativo = 1
        ORDER BY
            fb.DataRegistro DESC
        LIMIT 1";
    $resultQryUltimoFeedBack = mysqli_query($con, $qryUltimoFeedBack) or die (mysqli_error($resultqryUltimoFeedBack));
?>


<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?=$fetchEmpresa['NomeFantasia']?> - Estagiando</title>

        <!-- Bootstrap CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.3/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <style>
            body{
                background: rgb(245,245,245);
            }
            .container{
                background: #fff;
            }
        </style>
    </head>
    <body>
        <section class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="text-primary">
                        <h1 style="text-transform: uppercase;font-weight: bolder"><?=$fetchEmpresa['NomeFantasia']?></h1>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <h1>
                        <?php
                            for($i = 0; $i < $media; $i++){
                        ?>
                        <span style="font-size: 25px; color: yellow" class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        <?php
                            }
                        ?>
                        <?php
                            for($i = 5; $media < $i; $i--){
                        ?>
                            <span style="font-size: 25px; color: yellow" class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
                        <?php
                            }
                        ?>
                    </h1>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <hr/>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <!-- Links abas -->
                    <div class=" well well-sm  bg-white borderZero"  uib-dropdown >
                        <div class="btn-group date-block btn-group-justified font-small dropdown" data-toggle="buttons">
                            <label href="#about" data-toggle="tab" class="btn btn-default  next font-small semiBold" title="Informações sobre a empresa" style="font-size:12px; border-radius:0;">
                                Sobre
                            </label>
                            <label  href="#avaliacoes" data-toggle="tab" class="btn btn-default previous text-right font-small semiBold" title="Avaliações da empresa" style="font-size:12px;">
                                Avaliações (<?=$fetchTotalFeedback['Total']?>)
                            </label>
                            <label href="#vagas" data-toggle="tab" class="btn date-buttons btn-default text-right semiBold" title="Vagas publicadas" style="font-size:12px;" >
                                Vagas
                            </label>
                        </div>
                    </div>
                    <!-- /Links abas -->
                    <!-- Conteudo Abas -->
                    <div id="myTabContent" class="tab-content">

                        <!-- Sobre -->
                        <div class="tab-pane fade active in" id="about">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body" style="min-height: 100px;">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="text-default">
                                                        <h3><b>Descrição</b></h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</span>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="text-default">
                                                        <h3><b>Localização</b></h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Sobre -->

                        <!-- Avaliações -->
                        <div class="tab-pane fade in" id="avaliacoes">
                            <?php
                                while($fetchFeedback = mysqli_fetch_array($resultQryFeedback)){
                            ?>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body" style="min-height: 100px;">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                                            <span style="text-transform: uppercase; font-size: 25px"><?=utf8_encode($fetchFeedback['Titulo'])?></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 pull-right">
                                                            <span style="font-size: 25px"><?= date("d/m/Y H:i", strtotime($fetchFeedback['DataRegistro']))?></span>
                                                        </div>
                                                        <div style="margin-top: 15px;" class="col-xs-12">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                                                    Publicado por: <b><span><?=$fetchFeedback['Nome']?></b></span>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                                    <?php
                                                                        for($i = 0; $i < $fetchFeedback['Nota']; $i++){
                                                                    ?>
                                                                    <span style="font-size: 15px; color: yellow" class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                                    <?php
                                                                        }
                                                                    ?>
                                                                    <?php
                                                                        for($i = 5; $fetchFeedback['Nota'] < $i; $i--){
                                                                    ?>
                                                                        <span style="font-size: 15px; color: yellow" class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
                                                                    <?php
                                                                        }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                        <div style="margin-top: 15px" class="col-xs-12">
                                                            <span><?=$fetchFeedback['Comentario']?></span>
                                                        </div>
                                                    </div>   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                                }
                            ?>
                        </div>
                        <!-- /Avaliações -->

                        <!-- Vagas -->
                        <div class="tab-pane fade in" id="vagas">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body" style="min-height: 100px;">
                                            <!-- Vaga -->
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <span style="font-size: 25px"><b><a href="#">Motorista particular</a></b></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="margin-top: 15px" class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <span>Jaú - SP</span>
                                                            <hr/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="margin-top: 15px" class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-12"><span>Publicado há uma hora</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /Vaga -->
                                            <hr/>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <span style="font-size: 25px"><b><a href="#">Motorista particular</a></b></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="margin-top: 15px" class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <span>Jaú - SP</span>
                                                            <hr/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="margin-top: 15px" class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-12"><span>Publicado há uma hora</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Vagas -->
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-body" style="min-height: 100px;">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="text-default">
                                        <h3><b>Dados da empresa</b></h3>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h5>Razão Social: <?=$fetchEmpresa['RazaoSocial']?></h5>
                                </div>
                                <div class="col-xs-12">
                                    <h5>Nome Fantasia: <?=$fetchEmpresa['NomeFantasia']?></h5>
                                </div>
                                <div class="col-xs-12">
                                    <h5>Telefone: (<?=$fetchEmpresa['FoneDDD']?>) <?=$fetchEmpresa['FoneNumero']?></h5>
                                </div>
                                <div class="col-xs-12">
                                    <h5>Celular: (<?=$fetchEmpresa['CelularDDD']?>) <?=$fetchEmpresa['CelularNumero']?></h5>
                                </div>
                                <div class="col-xs-12">
                                    <h5>E-Mail: <?=$fetchEmpresa['Email']?></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div style="display: <?=$displayComentario?>" class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-default">
                                <h3><b>Avalie a Empresa</b></h3>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="titulo">Título</label>
                            <input type="text" name="titulo" id="titulo" class="form-control input-lg" >  <br/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="comentario">Digite seu comentário</label>
                            <textarea name="comentario" id="comentario" class="form-control input-lg" rows="3"></textarea>  <br/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="nota">Informe a nota</label>                  
                            <select name="nota" id="nota" class="form-control input-lg" required="required">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">  
                            <br/><button onClick="publicarComentario(<?=$codEmpresa?>, <?=$var_usuario_id?>)" type="button" class="btn btn-lg btn-large btn-block btn-primary">Publicar comentário</button>
                        </div>
                    </div>
                </div>
                <?php
                    if(mysqli_num_rows($resultQryUltimoFeedBack) > 0){
                        $fetchUltimoFeedback = mysqli_fetch_array($resultQryUltimoFeedBack);
                ?>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-default">
                                <h3><b>Última Avaliação</b></h3>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <span style="text-transform: uppercase; font-size: 25px"><?=utf8_encode($fetchUltimoFeedback['Titulo'])?></span>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <span style="font-size: 15px"><?= date("d/m/Y H:i", strtotime($fetchUltimoFeedback['DataRegistro']))?></span>
                        </div>
                        <div style="margin-top: 15px;" class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    Publicado por: <b><span><?=$fetchUltimoFeedback['Nome']?></b></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <?php
                                        for($i = 0; $i < $fetchUltimoFeedback['Nota']; $i++){
                                    ?>
                                    <span style="font-size: 15px; color: yellow" class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                    <?php
                                        }
                                    ?>
                                    <?php
                                        for($i = 5; $fetchUltimoFeedback['Nota'] < $i; $i--){
                                    ?>
                                        <span style="font-size: 15px; color: yellow" class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
                                    <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 15px" class="col-xs-12">
                            <span><?=$fetchUltimoFeedback['Comentario']?></span>
                        </div>
                    </div>
                </div>
                <?php
                    }
                ?>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-default">
                                <h3><b>Contato</b></h3>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="assunto">Assunto</label>
                            <input type="text" name="assunto" id="assunto" class="form-control input-lg"><br/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="mensagem">Mensagem</label>
                            <textarea name="mensagem" id="mensagem" class="form-control input-lg" rows="3"></textarea>
                        </div>
                    </div>
                    <div style="margin-top: 15px" class="row">
                        <div class="col-xs-12">
                            
                            <button type="button" class="btn btn-lg btn-large btn-block btn-primary">Enviar E-Mail</button>
                            
                        </div>
                    </div>
                </div>
            </div>
    
        </section>

        <script>
            function publicarComentario(idEmpresa, idAssinante){
                ocorrencia = "comentarioEmpresa";
                titulo = $("#titulo").val();
                comentario = $('#comentario').val();
                nota = $('#nota').val();
                $.post( 
                    "../publicarComentarioAjax.php", 
                    {
                        idEmpresa: idEmpresa.toString(),
                        idAssinante: idAssinante.toString(),
                        ocorrencia: ocorrencia.toString(),
                        titulo: titulo.toString(),
                        comentario: comentario.toString(),
                        nota: nota.toString(),
                    },
                    function( data ) {
                        if(data == "Sucesso")
                            window.open("login.php?msgs=Usuário registrado com sucesso!", "_self");
                    }
                );
            }
        </script>

        <!-- jQuery -->
        <script src="../js/jquery.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="../js/bootstrap.min.js"></script>
    </body>
</html>
