
<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Acesso ao Sistema</title>

		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
		<!-- <link href="css/login.css" rel="stylesheet"> -->
		
		<!------ Include the above in your HEAD tag ---------->

		<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

		<style>
			body, .input-lg{
				background: rgb(37,51,61);
			}
			.container{
				padding-top: 10%;
			}
			.border{
				border-left: 1px solid #ccc;
				height: 375px;
			}
			.text-primary > h1{
				color: rgb(110,218,161);
			}
			.form-control{
				/* border: 1px solid rgb(110,218,161); */
				border: 1px solid white;
			}
			.form-control:focus{
				box-shadow: 0 0 5px rgb(110,218,161);
				color: white;
				border: 1px solid black(110,218,161);
				outline-color: red !important;
			}
			.btn-primary{
				background: rgb(110,218,161);
				height: 60px;
			}
			.btn-primary:hover{
				background: rgb(110,250,161);
			}
			@media (max-width: 768px) {
				.border{
					display: none;
				}
				small{
					display: none;
				}
			}
		</style>
	</head>

	<body>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5"></div>
				<div class="col-xs-1">
					<div class="border">&nbsp;</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
					<div class="row">
						<div class="col-xs-12">
							<div class="text-primary">
								<h1>Acesso ao Sistema <small style="color: white">- Informe os dados e conecte-se</small></h1>
							</div>
						</div>
					</div>
					<hr/>
					<div class="omb_login">
						<div class="row omb_row-sm">
							<div class="col-xs-12 col-sm-12">	
								<form class="omb_loginForm" action="loginFuncao.php" autocomplete="off" method="POST">
									<input type="text" class="form-control input-lg" name="username" placeholder="Endereço de e-mail"><br/>
									<input  type="password" class="form-control input-lg" name="password" placeholder="Senha">
									<br/>
									<button onClick="loaderBotao()" id="btnEntrar" class="btn btn-lg btn-primary btn-block" type="submit"><strong><i class="fa fa-sign-in" aria-hidden="true"></i> CONECTAR</strong></button>
								</form>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</body>

	<script>
		function loaderBotao(){
			$("#btnEntrar").html("<strong><i class='fa fa-spinner fa-spin' aria-hidden='true'></i> Conectando...</strong>");
		}
	</script>
</html>