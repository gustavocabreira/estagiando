<?php
    include 'config.php';

    $email = $_POST['username'];
    $password = md5($_POST['password']);

    // Consulta o assinante que está logando
    $consultaEmail = "
        SELECT
            Id
            , Nome
            , Senha
        FROM
            assinantes
        WHERE
            Email = '".$email."'
    ";
    $resultEmail = mysqli_query($con, $consultaEmail);


    // Se esse email existir no sistema
    if(mysqli_num_rows($resultEmail) > 0){
        // Armazeno os dados da consulta em um array
        $fetchEmail = mysqli_fetch_array($resultEmail);

        // Se a senha digitada for igual a senha do cadastro (senha criptografada em md5)
        if($password == $fetchEmail['Senha']){
            // Inicio a sessão, armazeno os dados do cadastro do usuário e redireciono para o inicio
            session_start();
            $_SESSION['var_usuario_id'] = $fetchEmail['Id'];
            $_SESSION['var_usuario_nome'] = $fetchEmail['Nome'];
            $_SESSION['var_usuario_senha'] = $_POST['password'];
            $_SESSION['var_usuario_menu'] = "Assinante";
            header("Location: inicio.php");
        }
        else{
            header("Location: login.php?msgd=Senha inválida!");
        }
    }
    else{
        $consultaEmail = "
            SELECT
                Id
                , NomeFantasia
                , Senha
                , DataRegistro
            FROM
                empresas
            WHERE
                Email = '".$email."'
        ";
        $resultEmail = mysqli_query($con, $consultaEmail);

        if(mysqli_num_rows($resultEmail) > 0){
            // Armazeno os dados da consulta em um array
            $fetchEmail = mysqli_fetch_array($resultEmail);

            // Se a senha digitada for igual a senha do cadastro (senha criptografada em md5)
            if($password == $fetchEmail['Senha']){
                // Inicio a sessão, armazeno os dados do cadastro do usuário e redireciono para o inicio
                session_start();
                $_SESSION['var_usuario_id'] = $fetchEmail['Id'];
                $_SESSION['var_usuario_nome'] = $fetchEmail['NomeFantasia'];
                $_SESSION['var_usuario_senha'] = $_POST['password'];
                $_SESSION['var_usuario_menu'] = "Empresa";
                $_SESSION['var_usuario_dt_registro'] = $fetchEmail['DataRegistro'];
                header("Location: inicio.php");
            }
            else{
                header("Location: login.php?msgd=Senha inválida!");
            }
        }
        else{
            header("Location: login.php?msgd=E-Mail não encontrado!");
        }
    }
?>