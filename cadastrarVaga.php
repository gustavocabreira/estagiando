<?php
    include 'topo.php';
    $var_usuario_id = $_SESSION['var_usuario_id'];
?>

<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="css/wysihtml5.css">

<!-- Site wrapper -->
<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Cadastrar vaga
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="#">Vagas</a></li>
        <li class="active">Publicar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Informe os dados sobre as vagas</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <!-- <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button> -->
          </div>
        </div>
        <div class="box-body">
		  <div class="row">
		  	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			  <div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<label for="titulo">Título da Vaga</label>
					<input type="text" name="titulo" id="titulo" class="form-control input-lg" title="Informe o título da vaga"><Br/>
				</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<label for="titulo">De</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
						<input onKeyUp="moeda(this)" type="text" name="titulo" id="titulo" class="form-control input-lg" title="Limite inferior de salário">
					</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<label for="titulo">Até</label>
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
							<input onKeyUp="moeda(this)" type="text" name="titulo" id="titulo" class="form-control input-lg" title="Limite superior de salário">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<br/><label for="areas">Áreas</label>
						<select id="areas" class="form-control input-lg col-sm-12 col-xs-12 col-md-12 col-lg-12" name="states[]" multiple="multiple">
							<?php
								$consultaAreas = "
									SELECT
										*
									FROM
										areas
									ORDER BY
										Descricao
								";
								$resultAreas = mysqli_query($con, $consultaAreas) or die (mysqli_error($resultAreas));

								while($fetchAreas = mysqli_fetch_array($resultAreas)){
								?>
								<option value="<?=$fetchAreas['Id']?>"><?=utf8_encode($fetchAreas['Descricao'])?></option>
								<?php
								}
							?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<br/><label for="beneficios">Beneficios</label>
						<textarea name="beneficios" id="beneficios" class="form-control input-lg" rows="3" required="required"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<br/><label for="horario">Horário</label>
						<textarea name="horario" id="horario" class="form-control input-lg" rows="3" required="required"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<br/><label for="extras">Informações adicionais</label>
						<textarea name="extras" id="extras" class="form-control input-lg" rows="3" required="required"></textarea>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="row">
					<div class="col-xs-12">
						<label for="descricao">Descrição da vaga</label>
						<textarea id="descricao" class="form-control" rows="14" placeholder=""></textarea>
					</div>
				</div>
				<div style="margin-top: 15px" class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<label for="prazo">Prazo</label>
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="date" id="prazo" class="form-control input-lg" value="<?=date('Y-m-d', strtotime("+7 days"))?>">
						</div>
					</div>
				</div>
			</div>
		  </div>
          
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="row">
		  	<div class="col-xs-12">
				  <button type="button" class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> Publicar</button>
			  </div>
		  </div>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->
<script src="js/jquery.js"></script>
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" /> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<!-- CK Editor -->
<script src="bower_components/ckeditor/ckeditor.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src"js/wysihtml5.all.min.js"></script>
<script src="js/funcoes.js"></script>

<script>
	$(document).ready(function() {
		$('#areas').select2();
	});
</script>

<?php
    include 'rodape.php';
?>