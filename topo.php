<?php
    session_start();
    if(!isset($_SESSION['var_usuario_id'])){
        header("Location: login.php?msgw=É necessário estar conectado para acessar essa parte do sistema!");
    }
    else{
        include 'config.php';

        if($_SESSION['var_usuario_menu'] == "Empresa"){
            include 'topoEmpresa.php';
        }
        else if ($_SESSION['var_usuario_menu'] == "Assinante"){
            include 'topoAssinante.php';
        }
    }

?>