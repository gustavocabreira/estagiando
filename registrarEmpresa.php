<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Title Page</title>

        <!-- Bootstrap CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <script src="js/funcoes.js"></script>

        <style>
            .col-xs-12{
                margin-top: 15px;
            }
            .container{
                padding-bottom: 15px;
            }
        </style>

    </head>
    <body>
        <section class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="text-primary">
                        <h1>Cadastre-se!</h1>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <!-- Dados pessoais -->
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label for="txtRazaoSocial">Razão Social:*</label>
                            <input type="text" name="txtRazaoSocial" id="txtRazaoSocial" class="form-control input-lg">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label for="txtNomeFantasia">Nome Fantasia:*</label>
                            <input type="text" name="txtNomeFantasia" id="txtNomeFantasia" class="form-control input-lg">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label for="txtCnpj">CNPJ:*</label>
                            <input type="text" name="txtCnpj" id="txtCnpj" class="form-control input-lg">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label for="txtIe">Inscrição Estadual:*</label>
                            <input type="text" name="txtIe" id="txtIe" class="form-control input-lg">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label for="txtEmail">E-Mail:*</label>
                            <input type="email" name="txtEmail" id="txtEmail" class="form-control input-lg">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label for="txtTelefone">Telefone:*</label>
                            <input type="text" name="txtTelefone" id="txtTelefone" class="form-control input-lg" maxlength="14">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label for="txtCelular">Celular:*</label>
                            <input type="text" name="txtCelular" id="txtCelular" class="form-control input-lg" maxlength="15">
                        </div>
                    </div>
                    <!-- Fim dados pessoais -->
                    <hr/>
                    <!-- Endereço -->
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label for="txtCep">CEP:*</label>
                            <input type="text" name="txtCep" id="txtCep" class="form-control input-lg" onBlur="buscaCep()">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                            <label for="txtEndereco">Endereço:*</label>
                            <input type="text" name="txtEndereco" id="txtEndereco" class="form-control input-lg" readonly>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label for="txtNumero">Número:*</label>
                            <input type="text" name="txtNumero" id="txtNumero" class="form-control input-lg">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <label for="txtBairro">Bairro:*</label>
                            <input type="text" name="txtBairro" id="txtBairro" class="form-control input-lg" readonly>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <label for="txtComplemento">Complemento:</label>
                            <input type="text" name="txtComplemento" id="txtComplemento" class="form-control input-lg">
                         </div>
                         <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
                            <label for="txtCidade">Cidade:*</label>
                            <input type="text" name="txtCidade" id="txtCidade" class="form-control input-lg" readonly>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
                            <label for="txtUf">UF:*</label>
                            <input type="text" name="txtUf" id="txtUf" class="form-control input-lg" readonly>
                        </div>
                    </div>       
                    <!-- Fim do Endereço  -->
                    <hr/>
                    <!-- Senha -->
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label for="txtSenha">Senha:* <span id="retornoSenha"></span></label>
                            <input maxlength="16" type="password" name="txtSenha" id="txtSenha" class="form-control input-lg" onKeyUp="validarSenha(this.value)">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <label for="txtConfSenha">Confirmar Senha:* <span id="retornoConfirmacao"></span></label>
                            <input maxlength="16" type="password" name="txtConfSenha" id="txtConfSenha" class="form-control input-lg" onKeyUp="validarConfirmacaoSenha(this.value)">
                        </div>
                    </div>
                    <!-- Fim senha -->
                    <div class="row">
                        <div class="col-xs-12">                     
                            <button onClick="cadastrar()" type="button" class="btn btn-large btn-block btn-success btn-lg"><strong>Cadastrar</strong></button>                 
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    </body>

    <script>
    
        /*
            Função que valida a força da senha
        */
        function validarSenha(senha){
            forca = 0;
            if((senha.length >= 4) && (senha.length <= 7)){
                forca += 10;
            }else if(senha.length>7){
                forca += 25;
            }
            if(senha.match(/[a-z]+/)){
                forca += 10;
            }
            if(senha.match(/[A-Z]+/)){
                forca += 20;
            }
            if(senha.match(/d+/)){
                forca += 20;
            }
            if(senha.match(/W+/)){
                forca += 25;
            }
            
            if(forca == 0){
                forca = "Insuficiente";
                cor = "text-danger";
                $("#txtConfSenha").prop("disabled",true);
                $("#txtConfSenha").val("");
            }
            else if(forca <= 25){
                forca = "Fraca";
                cor = "text-warning";
                $("#txtConfSenha").prop("disabled",true);
                $("#txtConfSenha").val("");
            }
            else if(forca <= 50){
                forca = "Normal";
                cor = "text-primary";
                $("#txtConfSenha").prop("disabled",false);
            }
            else{
                forca = "Alta";
                cor = "text-success";
                $("#txtConfSenha").prop("disabled",false);
            }
            
            $("#retornoSenha").html("<span id='forcaSenha' class="+cor+">"+forca+"</span>");
        }

        /*
            Função que compara a senha e a confirmação dela
        */
        function validarConfirmacaoSenha(confirmacao){
            senha = $("#txtSenha").val();

            if(senha != confirmacao){
                $("#retornoConfirmacao").html("<span id='validarConfirmacao' class='text-danger'>Senhas não coincidem!</span>");
            }
            else{
                $("#retornoConfirmacao").html("<span id='validarConfirmacao' class='text-success'>Senha validada!</span>");
            }
        }

        /*
            Função que valida os campos durante o cadastro
        */
        function validarCampos(){
            
            razao = $("#txtRazaoSocial").val();
            if(razao == ""){
                $.alert("O campo RAZÃO SOCIAL não pode estar em branco!");
                $("#txtRazaoSocial").focus();
                return false;
            }

            fantasia = $("#txtNomeFantasia").val();
            if(fantasia == ""){
                $.alert("O campo NOME FANTASIA não pode estar em branco!");
                $("#txtNomeFantasia").focus();
                return false;
            }

            cnpj = $("#txtCnpj").val();
            if(cnpj == ""){
                $.alert("O campo CNPJ não pode estar em branco!");
                $("#txtCnpj").focus();
                return false;
            }

            ie = $("#txtIe").val();
            if(ie == ""){
                $.alert("O campo INSCRIÇÃO ESTADUAL não pode estar em branco!");
                $("#txtIe").focus();
                return false;
            }
            
            email = $("#txtEmail").val();
            if(email == ""){
                $.alert("O campo E-MAIL não pode estar em branco!");
                $("#txtEmail").focus();
                return false;
            }

            telefone = $("#txtTelefone").val();
            if(telefone == ""){
                $.alert("O campo TELEFONE não pode estar em branco!");
                $("#txtTelefone").focus();
                return false;
            }
            
            celular = $("#txtCelular").val();
            if(celular == ""){
                $.alert("O campo CELULAR não pode estar em branco!");
                $("#txtCelular").focus();
                return false;
            }

            cep = $("#txtCep").val();
            if(cep == ""){
                $.alert("O campo CEP não pode estar em branco!");
                $("#txtCep").focus();
                return false;
            }
            
            endereco = $("#txtEndereco").val();
            if(endereco == ""){
                $.alert("O campo ENDERECO não pode estar em branco!");
                $("#txtEndereco").focus();
                return false;
            }

            numero = $('#txtNumero').val();
            if(numero == ""){
                $.alert("O campo NUMERO não pode estar em branco!");
                $("#txtNumero").focus();
                return false;
            }
            
            bairro = $('#txtBairro').val();
            if(bairro == ""){
                $.alert("O campo BAIRRO não pode estar em branco!");
                $("#txtBairro").focus();
                return false;
            }
            
            cidade = $('#txtCidade').val();
            if(cidade == ""){
                $.alert("O campo CIDADE não pode estar em branco!");
                $("#txtCidade").focus();
                return false;
            }

            uf = $('#txtUf').val();
            if(uf == ""){
                $.alert("O campo UF não pode estar em branco!");
                $("#txtUf").focus();
                return false;
            }

            senha = $('#txtSenha').val();
            if(senha == ""){
                $.alert("O campo SENHA não pode estar em branco!");
                $("#txtSenha").focus();
                return false;
            }

            confSenha = $('#txtConfSenha').val();
            if(confSenha == ""){
                $.alert("O campo CONFIRMAR SENHA não pode estar em branco!");
                $("#txtUf").focus();
                return false;
            }
        }

        /*
            Função que envia as informações para a página de função quando clicar no botão cadastrar
        */
        function cadastrar(){
            validar = validarCampos();

            /*
                Caso algum campo seja inválido (esteja em branco)
            */
            if(validar == false)
                return false;

            razao = $("#txtRazaoSocial").val();
            fantasia = $("#txtNomeFantasia").val();
            cnpj = $("#txtCnpj").val();
            ie = $("#txtIe").val();
            email = $("#txtEmail").val();
            confEmail = $("#txtConfEmail").val();
            telefone = $("#txtTelefone").val();
            celular = $("#txtCelular").val();
            cep = $("#txtCep").val();
            endereco = $("#txtEndereco").val();
            numero = $('#txtNumero').val();
            bairro = $('#txtBairro').val();
            complemento = $('#txtComplemento').val();
            cidade = $('#txtCidade').val();
            uf = $('#txtUf').val();
            senha = $('#txtSenha').val();
            confSenha = $('#txtConfSenha').val();

            // Se os campos de senha forem diferentes
            if(senha != confSenha){
                $.alert("As senhas devem ser iguais!");
                $("#txtConfSenha").focus();
                return false;
            }

            $.post( 
                "registrarEmpresaAjax.php", 
                {
                    razao: razao.toString(),
                    fantasia: fantasia.toString(),
                    cnpj: cnpj.toString(),
                    ie: ie.toString(),
                    email: email.toString(),
                    telefone: telefone.toString(),
                    celular: celular.toString(),
                    cep: cep.toString(),
                    endereco: endereco.toString(),
                    numero: numero.toString(),
                    bairro: bairro.toString(),
                    complemento: complemento.toString(),
                    cidade: cidade.toString(),
                    uf: uf.toString(),
                    senha: senha.toString(),
                },
                function( data ) {
                    if(data == "Sucesso") 
                        window.open("login.php?msgs=Empresa cadastrada com sucesso!", "_self");
                    else{
                        $.alert("Erro ao se registrar. Por favor, contate o suporte.");
                        return false;
                    }
                    
                }
            );
        }
    </script>
</html>