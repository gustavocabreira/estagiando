<?php
    include 'config.php';

    $var_usuario_id = $_SESSION['var_usuario_id'];
    $var_usuario_nome = $_SESSION['var_usuario_nome'];
    // $var_usuario_sobrenome = $_SESSION['var_usuario_sobrenome'];
    // $var_usuario_foto = $_SESSION['var_usuario_foto'];
    $var_usuario_dt_registro = $_SESSION['var_usuario_dt_registro'];  

    if($var_usuario_id == null)
      header("Location: login.php?msgs=É necessário realizar login!");
?>

<html>
    <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Estagiando</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.7 -->
      <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
      <!-- AdminLTE Skins. Choose a skin from the css/skins
           folder instead of downloading all of them to reduce the load. -->
      <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    
      <!-- Google Font -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin-red sidebar-mini">

<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>F</b>CR</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Estagiando</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        <!-- Notifications: style can be found in dropdown.less -->
        <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-users"></i>
              <span class="label label-warning"></span>
            </a>
            <ul class="dropdown-menu">
              <li>
                 <ul class="menu">
                  <li>
                    <a href="convitesPendentes.php">
                      <i class="fa fa-user-plus text-aqua"></i> Você tem novos convites de amizade
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="user_pics/" class="user-image" alt="User Image">
              <span class="hidden-xs"><?=$var_usuario_nome?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="user_pics/" class="img-circle" alt="User Image">

                <p>
                  <?=$var_usuario_nome ?>
                  <small>Membro desde <?=date("M", strtotime($var_usuario_dt_registro))?>, <?=date("Y", strtotime($var_usuario_dt_registro))?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="profile.php" class="btn btn-default btn-flat">Perfil</a>
                </div>
                <div class="pull-right">
                  <a href="sair.php" class="btn btn-default btn-flat">Sair</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="user_pics/" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?=$var_usuario_nome?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        
        <li><a href="inicio.php"><i class="fa fa-home"></i> <span>Painel Inicial</span></a></li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-credit-card"></i>
            <span>Cartões</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="cadCartaon.php"><i class="fa fa-circle-o"></i> Cadastrar</a></li>
            <li><a href="cadDespesa.php"><i class="fa fa-circle-o"></i> Gerenciar</a></li>            
            <li><a href="cadDespesa.php"><i class="fa fa-circle-o"></i> Relatório</a></li>            
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-shopping-cart"></i>
            <span>Despesas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="cadDespesa.php"><i class="fa fa-circle-o"></i> Cadastrar</a></li>
            <li><a href="cadDespesa.php"><i class="fa fa-circle-o"></i> Gerenciar</a></li>            
            <li><a href="cadDespesa.php"><i class="fa fa-circle-o"></i> Relatório</a></li>            
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dollar"></i>
            <span>Entradas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="cadEntrada.php"><i class="fa fa-circle-o"></i> Cadastrar</a></li>
            <li><a href="cadDespesa.php"><i class="fa fa-circle-o"></i> Gerenciar</a></li>            
            <li><a href="cadDespesa.php"><i class="fa fa-circle-o"></i> Relatório</a></li>            
          </ul>
        </li>
		
		<li class="treeview">
          <a href="#">
            <i class="fa fa-line-chart"></i>
            <span>Metas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="cadEntrada.php"><i class="fa fa-circle-o"></i> Cadastrar</a></li>
            <li><a href="cadDespesa.php"><i class="fa fa-circle-o"></i> Gerenciar</a></li>            
            <li><a href="cadDespesa.php"><i class="fa fa-circle-o"></i> Relatório</a></li>            
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-area-chart"></i>
            <span>Relatórios</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="cadDespesa.php"><i class="fa fa-circle-o"></i> Despesas</a></li>
            <li><a href="cadEntrada.php"><i class="fa fa-circle-o"></i> Entradas</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Amizades</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="enviarConviteAmizade.php"><i class="fa fa-user-plus"></i> Adicionar</a></li>
            <li><a href="convitesPendentes.php"><i class=""></i> Convite Pendente</a></li>
            <li><a href="visualizarAmizades.php"><i class="fa fa-eye"></i> Ver Amizades</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  
  