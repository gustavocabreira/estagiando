<?php
    /*
        ---------------------Bloco de Comentários---------------------
        A página que faz referência à essa é a registrarEmpresa.php
        --------------------------------------------------------------
    */  

    include 'config.php';

    /*
        Função que valida os caracteres especiais (formatação de acentos e aspas)
    */
    function validarCampos($valor){
        $valor = utf8_decode($valor);
        $valor = addslashes($valor);
        return $valor;
    }

    /*
        Função que valida os caracteres especiais e espaços que vêm da máscara de telefone
    */
    function validarTelefone($valor){
        $valor = str_replace("(",'',$valor);
        $valor = str_replace(')','',$valor);
        $valor = str_replace("-",'',$valor);
        $valor = str_replace(' ','',$valor);
        return $valor;
    }

    $razao = validarCampos($_POST['razao']);
    $fantasia = validarCampos($_POST['fantasia']);
    $cnpj = validarCampos($_POST['cnpj']);
    $ie = validarCampos($_POST['ie']);
    $email =  validarCampos($_POST['email']);
    
    $telefone =  validarTelefone($_POST['telefone']);
    // Separando o DDD do Número
    $ddd = substr($telefone, 0 , 2); 
    $fone = substr($telefone, 2); 

    $celular =  validarTelefone($_POST['celular']);
    // Separando o DDD do Número
    $dddCelular = substr($celular, 0 , 2); 
    $numeroCelular = substr($celular, 2); 
    
    $cep =  validarCampos($_POST['cep']);
    $endereco =  validarCampos($_POST['endereco']);
    $numero =  validarCampos($_POST['numero']);
    $bairro =  validarCampos($_POST['bairro']);

    // Definindo o valor padrão da váriavel que insere no banco o complemento
    $string_complemento = null;

    // Caso o complemento não esteja em branco
    if(isset($_POST['complemento'])){
        $complemento =  validarCampos($_POST['complemento']);
        $string_complemento = ", Complemento = '".$complemento."'";
    }

    $cidade =  validarCampos($_POST['cidade']);
    $uf =  validarCampos($_POST['uf']);
    $senha =  utf8_decode($_POST['senha']);

    $var_data_agora = date("Y-m-d H:i:s");

    $senha = md5($senha);

    $insert = "
        INSERT INTO
            empresas
        SET
            RazaoSocial = '".$razao."'
            , NomeFantasia = '".$fantasia."'
            , Cnpj = '".$cnpj."'
            , InscricaoEstadual = '".$ie."'
            , Email = '".$email."'
            , FoneDDD = ".$ddd."
            , FoneNumero = ".$numero."
            , CelularDDD = ".$dddCelular."
            , CelularNumero = ".$numeroCelular."
            , Cep = ".$cep."
            , Endereco = '".$endereco."'
            , Numero = ".$numero."
            , Bairro = '".$bairro."'
            ".$string_complemento."
            , Cidade = '".$cidade."'
            , Senha = '".$senha."'
            , DataRegistro = '".$var_data_agora."'
    ";
    $resultInsert = mysqli_query($con, $insert);

    // Se os dados forem inseridos no banco
    if($resultInsert)
        echo "Sucesso";
    else
        echo "Erro";
?>